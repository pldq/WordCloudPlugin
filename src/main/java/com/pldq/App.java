package com.pldq;

import com.pldq.wordcloud.WordCloudPlugin;

public class App {
    public static void main( String[] args ) {
        if (args.length > 0) {
            for (String needDealPathName : args) {
                WordCloudPlugin wordCloudPlugin = new WordCloudPlugin(needDealPathName);
                process(wordCloudPlugin);
            }
        } else {
            WordCloudPlugin wordCloudPlugin = new WordCloudPlugin();
            process(wordCloudPlugin);
        }
    }

    private static void process(WordCloudPlugin wordCloudPlugin) {
        wordCloudPlugin.process();
    }
}
