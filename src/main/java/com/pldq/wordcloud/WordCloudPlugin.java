package com.pldq.wordcloud;

import java.awt.Color;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import com.huaban.analysis.jieba.JiebaSegmenter;
import com.kennycason.kumo.CollisionMode;
import com.kennycason.kumo.WordCloud;
import com.kennycason.kumo.WordFrequency;
import com.kennycason.kumo.bg.PixelBoundryBackground;
import com.kennycason.kumo.font.FontWeight;
import com.kennycason.kumo.font.KumoFont;
import com.kennycason.kumo.font.scale.LinearFontScalar;
import com.kennycason.kumo.palette.ColorPalette;
import com.pldq.wordcloud.factory.SettingsFactory;
import com.pldq.wordcloud.lists.FixedSizeSortLinkedList;
import com.pldq.wordcloud.settings.Settings;

public class WordCloudPlugin {

    private File needDealFile;

    public WordCloudPlugin() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please input the path of need to deal:");
        String needDealPathName = scanner.nextLine();
        scanner.close();
        openFile(needDealPathName);
    }

    public WordCloudPlugin(String needDealPathName) {
        openFile(needDealPathName);
    }

    public void openFile(String needDealPathName) {
        needDealFile = new File(needDealPathName);
        if (!needDealFile.isFile()) {
            System.out.println("The path is not a file!");
            needDealFile = null;
        }
    }

    public void process() {
        if (needDealFile == null) {
            return;
        }

        // 加载 Settings 设置
        Settings settings = SettingsFactory.getDefaultSettingsFactory().create();

        JiebaSegmenter jiebaSegmenter = new JiebaSegmenter();
        HashMap<String, Integer> wordRateMap = new HashMap<>();
        long startTimeStamp = System.currentTimeMillis();
        try (BufferedReader bufferedReader = new BufferedReader(
            new InputStreamReader(new FileInputStream(needDealFile), "UTF-8"))) {
            bufferedReader.lines()
            .filter(line -> line.trim().length() > 0)
            .forEach(line -> {
                jiebaSegmenter.sentenceProcess(line)
                .stream()
                .filter(word -> word.trim().length() >= settings.getMinWordLength())
                .forEach(word -> {
                    int rate = 1;
                    Integer oldRate = wordRateMap.get(word);
                    if (oldRate != null) {
                        rate = oldRate + 1;
                    }
                    wordRateMap.put(word, rate);
                });
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("Segmenter's work is over! Map's size = " 
                + wordRateMap.size() + ", used time = "
                + (System.currentTimeMillis() - startTimeStamp)
                + " ms."
            );
        }

        List<WordFrequency> words = new FixedSizeSortLinkedList<>(
            settings.getShowWordSize()
        );
        wordRateMap.forEach((key, value) -> {
            words.add(new WordFrequency(key, value));
        });

        if (settings.isCreateRateFile()) {
            createRateFile(words);
        }

        WordCloud wordCloud = new WordCloud(
            new Dimension(
                settings.getSize().getWidth(),
                settings.getSize().getHeight()
            ), CollisionMode.PIXEL_PERFECT
        );
        wordCloud.setPadding(settings.getPadding());
        wordCloud.setBackgroundColor(
            settings.getBackgroundColor() == null ?
            null : new Color(Integer.parseInt(settings.getBackgroundColor(), 16))
        );

        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(
                settings.getBackgroundPath()
            );
            if (inputStream == null) {
                wordCloud.setBackground(new PixelBoundryBackground(
                    settings.getBackgroundPath()
                ));
            } else {
                wordCloud.setBackground(new PixelBoundryBackground(inputStream));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    
        wordCloud.setFontScalar(new LinearFontScalar(
            settings.getFont().getMinFont(), 
            settings.getFont().getMaxFont()
        ));
        Color[] colors = new Color[settings.getColorPalettes().length];
        for (int i = 0; i < colors.length; i++) {
            colors[i] = new Color(
                Integer.parseInt(settings.getColorPalettes()[i], 16)
            );
        }
        wordCloud.setColorPalette(new ColorPalette(colors));
        wordCloud.setKumoFont(new KumoFont(
            settings.getFont().getType(),
            FontWeight.valueOf(settings.getFont().getWeight())
        ));
        wordCloud.build(words);

        try (OutputStream outputStream = new FileOutputStream(new File(needDealFile.getParent(), needDealFile.getName() + ".png"))) {
            wordCloud.writeToStreamAsPNG(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createRateFile(List<WordFrequency> words) {
        try (
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                new File(needDealFile.getParent(), needDealFile.getName() + ".rate")
            )))
        ) {
            for (WordFrequency word : words) {
                bufferedWriter.write(
                    word.getWord() + " " + 
                    word.getFrequency() + "\r\n"
                );
            }
            bufferedWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}