package com.pldq.wordcloud.factory;

import com.pldq.wordcloud.settings.Settings;

public interface SettingsFactory {

    Settings create();

    public static SettingsFactory getDefaultSettingsFactory() {
        return new DefaultSettingsFactory();
    }
}