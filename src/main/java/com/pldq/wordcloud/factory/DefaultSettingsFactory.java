package com.pldq.wordcloud.factory;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;

import javax.imageio.ImageIO;

import com.google.gson.Gson;
import com.pldq.wordcloud.settings.Settings;
import com.pldq.wordcloud.settings.Size;

public class DefaultSettingsFactory implements SettingsFactory {

    private static final String FILE_NAME = "settings.json";
    private Gson gson = new Gson();

    @Override
    public Settings create() {
        InputStream inputStream = Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        try (InputStreamReader reader = new InputStreamReader(inputStream)) {
            return getBackgroundSize(gson.fromJson(reader, Settings.class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取背景图片大小，从相对路径和绝对路径寻找背景图片
     * 
     */
    private Settings getBackgroundSize(Settings settings) throws IOException {
        String backgroundPath = settings.getBackgroundPath();
        File absoluteBackgroundFile = new File(backgroundPath);
        BufferedImage bufferedImage = Objects.requireNonNull(absoluteBackgroundFile.isFile() ? 
            ImageIO.read(absoluteBackgroundFile) : 
            ImageIO.read(
                getClass().getClassLoader().getResourceAsStream(settings.getBackgroundPath())
            )
        );
        settings.setSize(new Size(bufferedImage.getWidth(), bufferedImage.getHeight()));
        return settings;
    }
}