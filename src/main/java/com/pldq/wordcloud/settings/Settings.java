package com.pldq.wordcloud.settings;

public class Settings {
    private Size size;
    private boolean createRateFile;
    private int minWordLength;
    private int showWordSize;
    private String backgroundPath;
    private String backgroundColor;
    private int padding;
    private String[] colorPalettes;
    private Font font;

    public Size getSize() { return size; }
    public void setSize(Size value) { this.size = value; }

    public boolean isCreateRateFile() { return createRateFile; }
    public void setCreateRateFile(boolean value) { this.createRateFile = value; }

    public int getMinWordLength() { return minWordLength; }
    public void setMinWordLength(int value) { this.minWordLength = value; }

    public int getShowWordSize() { return showWordSize; }
    public void setShowWordSize(int showWordSize) { this.showWordSize = showWordSize; }

    public String getBackgroundPath() { return backgroundPath; }
    public void setBackgroundPath(String value) { this.backgroundPath = value; }

    public String getBackgroundColor() { return backgroundColor; }
    public void setBackgroundColor(String value) { this.backgroundColor = value; }

    public int getPadding() { return padding; }
    public void setPadding(int value) { this.padding = value; }

    public String[] getColorPalettes() { return colorPalettes; }
    public void setColorPalettes(String[] value) { this.colorPalettes = value; }

    public Font getFont() { return font; }
    public void setFont(Font value) { this.font = value; }

}
