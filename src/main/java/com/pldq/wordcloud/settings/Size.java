package com.pldq.wordcloud.settings;

public class Size {
    private int width;
    private int height;

    public Size(int width, int height) {
        this.width = width;
        this.height = height;
	}
	public int getWidth() { return width; }
    public void setWidth(int value) { this.width = value; }

    public int getHeight() { return height; }
    public void setHeight(int value) { this.height = value; }
}