package com.pldq.wordcloud.settings;

public class Font {
    private int minFont;
    private int maxFont;
    private String type;
    private String weight;

    public int getMinFont() { return minFont; }
    public void setMinFont(int value) { this.minFont = value; }

    public int getMaxFont() { return maxFont; }
    public void setMaxFont(int value) { this.maxFont = value; }

    public String getType() { return type; }
    public void setType(String value) { this.type = value; }

    public String getWeight() { return weight; }
    public void setWeight(String value) { this.weight = value; }
}