package com.pldq.wordcloud.lists;

import java.util.AbstractSequentialList;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * 基于双向链表的固定大小的自动排序 List，到达指定大小后添加元素会丢弃链表尾部的节点
 * 
 */
public class FixedSizeSortLinkedList<E extends Comparable<E>> 
extends AbstractSequentialList<E> implements List<E> {

    private static class Node<E> {
        E data;
        Node<E> prev;
        Node<E> next;

        Node(Node<E> prev, E data, Node<E> next) {
            this.prev = prev;
            this.data = data;
            this.next = next;
        }
    }

    private final int fixedSize;
    private int currentSize;

    private Node<E> first;
    private Node<E> last;

    public FixedSizeSortLinkedList(int fixedSize) {
        this.fixedSize = fixedSize;
        this.currentSize = 0;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("fixed size = ").append(fixedSize).append("\r\n");
        stringBuilder.append("current size = ").append(currentSize).append("\r\n");
        for (Node<E> node = first; node != null; node = node.next) {
            stringBuilder.append(node.data);
            stringBuilder.append(node.next == null ? "\r\n" : "->");
        }
        for (Node<E> node = last; node != null; node = node.prev) {
            stringBuilder.append(node.data);
            stringBuilder.append(node.prev == null ? "\r\n" : "<-");
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean add(E e) {
        if (fixedSize <= 0) {
            return true;
        }
        if (currentSize == 0) {
            // 空列表
            first = last = new Node<>(null, e, null);
            currentSize++;
        } else {
            Node<E> node;
            for (node = first; node != null; node = node.next) {
                if (e.compareTo(node.data) < 0) {
                    Node<E> newNode = new Node<>(node.prev, e, node);
                    if (first == node) {
                        first = newNode;
                    } else {
                        node.prev.next = newNode;
                    }
                    node.prev = newNode;
                    if (currentSize >= fixedSize) {
                        // List 大小超过指定大小，删除最后一个
                        last = last.prev;
                        last.next = null;
                    } else {
                        currentSize++;
                    }
                    break;
                }
            }
            // 需要加入在最后一个
            if (node == null && currentSize < fixedSize) {
                Node<E> newNode = new Node<>(last, e, null);
                newNode.prev.next = newNode;
                last = newNode;
                currentSize++;
            }
        }
        return true;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        checkPositionIndex(index);
        return new ListItr(index);
    }

    private class ListItr implements ListIterator<E> {

        private Node<E> lastReturned;
        private Node<E> next;
        private int nextIndex;

        ListItr(int index) {
            next = (index == currentSize) ? null : node(index);
            nextIndex = index;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < currentSize;
        }

        @Override
        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.data;
        }

        @Override
        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        @Override
        public E previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            lastReturned = next = (next == null) ? last : next.prev;
            nextIndex--;
            return lastReturned.data;
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() {
            if (lastReturned == null) {
                throw new IllegalStateException();
            }
            Node<E> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned) {
                next = lastNext;
            } else {
                nextIndex--;
            }
            lastReturned = null;
        }

        @Override
        public void set(E e) {
            throw new IllegalStateException();
        }

        @Override
        public void add(E e) {
            throw new IllegalStateException();
        }

        @Override
        public void forEachRemaining(Consumer<? super E> action) {
            Objects.requireNonNull(action);
            while (nextIndex < currentSize) {
                action.accept(next.data);
                lastReturned = next;
                next = next.next;
                nextIndex++;
            }
        }
    }

    private String outOfBoundsMsg(int index) {
        return "Index: "+index+", Size: "+currentSize;
    }

    private E unlink(Node<E> x) {
        final E data = x.data;
        final Node<E> prev = x.prev;
        final Node<E> next = x.next;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }

        x.data = null;
        currentSize--;
        return data;
    }

    private Node<E> node(int index) {
        if (index < (currentSize >> 1)) {
            Node<E> x = first;
            for (int i = 0; i < index; i++) {
                x = x.next;
            }
            return x;
        } else {
            Node<E> x = last;
            for (int i = currentSize - 1; i > index; i--) {
                x = x.prev;
            }
            return x;
        }
    }

    private void checkPositionIndex(int index) {
        if (!isPositionIndex(index)) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private boolean isPositionIndex(int index) {
        return index >= 0 && index < currentSize;
    }

    @Override
    public int size() {
        return currentSize;
    }

    @Override
    public void sort(Comparator<? super E> c) {
        return;
    }
}